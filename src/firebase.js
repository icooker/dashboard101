import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

var config = {
    apiKey: "AIzaSyCcozUYAO_YNWJfxsETpV0BE3rjoIzmeD8",
    authDomain: "dashboard101-cfbce.firebaseapp.com",
    databaseURL: "https://dashboard101-cfbce.firebaseio.com",
    projectId: "dashboard101-cfbce",
    storageBucket: "dashboard101-cfbce.appspot.com",
    messagingSenderId: "564313089596"
};

firebase.initializeApp(config);
const firestore = firebase.firestore();
firestore.settings({ timestampsInSnapshots: true });

export const firestoreDb = firestore;
export const firebaseStorage = firebase.storage();
