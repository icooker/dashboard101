import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import HighchartsVue from 'highcharts-vue'

import { registerBaseComponents } from '@/utils';
registerBaseComponents();

Vue.config.productionTip = false;
Vue.use(HighchartsVue);

const resource = require('vue-resource');
Vue.use(resource);

Vue.prototype.$http = axios

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
